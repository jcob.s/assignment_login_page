# assignment_login_page

Ux-designer assignment (Eon Reality)

Important key features:
- Latest HTML & CSS standards
- Responsive design using Bootstrap 4
- Cross-browser compatibility

Extra credits:
- Email input field validation, email address format.
- Password minLength set to 6 characters.
- Forgot password & Sign up member links are added tooltip text.
- Input fields float label when active (kind of visualization).
- Added shortcut icon.